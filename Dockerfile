FROM ekidd/rust-musl-builder AS builder

ARG MDBOOK_VERSION="0.3.1"
LABEL version=$MDBOOK_VERSION
ARG LINKCHECK_VERSION="0.4.0"

RUN cargo install mdbook --vers ${MDBOOK_VERSION}
RUN cargo install mdbook-linkcheck --vers ${LINKCHECK_VERSION}

FROM alpine:latest

COPY --from=0 \
    /home/rust/.cargo/bin/mdbook* \
    /usr/local/bin/

RUN mdbook --version && mdbook-linkcheck --version
